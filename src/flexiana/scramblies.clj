(ns flexiana.scramblies
  (:gen-class)
  (:require
    [clojure.spec.alpha :as s]))

(defn char-freq
  "Given a string returns a map with each character frequencies"
  ([word] (char-freq word {}))
  ([[char & word] freqs]
   (if-not char
     freqs
     (recur word (assoc freqs char (+ (get freqs char 0) 1))))))

(s/def :scramble/str (s/and string? #(re-matches #"[a-z]*" %)))

(defn scramble?
  "Checks if chars in str1 can be rearranged to form str2"
  [str1 str2]
  {:pre [(s/valid? :scramble/str str1)
         (s/valid? :scramble/str str2)]}
  (if (and (> (count str1) 0) (= (count str2) 0))
    false
    (let [freq1 (char-freq str1)
         freq2 (char-freq str2)
         sq (seq freq2)]
     (every? (fn [[k v]] (>= (get freq1 k 0) v)) sq))))

(defn -main
  [& args])

;; journal
;; Rich comment block with redefined vars ignored
#_:clj-kondo/ignore
(comment

  (let [[c & word] "words"] word)
  ;; word is a list of chars I may don't like that

  (def word "rekqodlww")
  (def freq1 (char-freq word))
  ;; seems it's working cuz destructuring works for lists of course

  ;; validation
  (def valid (scramble? "" ""))
  (def valid (scramble? "cedewaraaossoqqyt" "codewars"))
  (def invalidLen (scramble? "cedewaraaossoqqyt" ""))
  (def invalidNum (scramble? 10 15))
  (def invalidCase (scramble? "Abc" "Abc"))
  (def invalidSymbols (scramble? "abc." "abc."))) ;; End of rich comment block
