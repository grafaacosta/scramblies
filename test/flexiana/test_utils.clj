(ns flexiana.test_utils
  (:require
   [clojure.string :refer [join]]))

(defn rand-str
  "Generates a random string with [a-z]"
  [len]
  (apply str (take len (repeatedly #(char (+ (rand 26) 97))))))

(defn str-remove
  "remove pos in string"
  [pos s]
  (str (subs s 0 pos) (subs s (+ 1 pos))))

(defn str-add
  "add randomly to string"
  ([s] (str-add s 1))
  ([s times]
   (if (< times 0)
     s
     (let [c (char (+ (rand 26) 97))
           l (count s)
           r (Math/floor (rand l))]
       (recur (str (subs s 0 r) c (subs s r)) (- times 1))))))

(defn mix-str
  "mixes randomly a string"
  ([s] (mix-str s []))
  ([s mix]
   (if (empty? s)
     (join "" mix)
     (let [l (count s)
           r (Math/floor (rand l))]
       (recur (str-remove r s) (conj mix (get s r)))))))

(defn generate-true-result
  "Generates a pair str1 str2 of the same len that is a success case for scramble?"
  [len]
  (let [base (rand-str len)
        str2 (mix-str base)]
    [base str2]))

(defn generate-asym-true-result
  "Generates a pair str1 str2 of different len that is a success case for scramble?"
  [blen slen]
  (let [base (rand-str blen)
        str2 (mix-str base)]
    [(str-add base slen) str2]))

(defn generate-false-result
  "Generates a pair str1 str2 of different len that is a failed case for scramble?"
  [blen slen]
  (let [[str1 str2] (generate-asym-true-result blen slen)]
    [str2 str1]))

;; Rich comment block with redefined vars ignored
#_{:clj-kondo/ignore [:redefined-var]}
(comment

  ;; is it from 0 to 20?
  (rand 20)

  (def mix1 "hello")

  (mix-str mix1)
  (generate-true-result 0)
  (generate-true-result 10)

  ;; returns uppercase
  (char (+ (rand 26) 65))

  ;; shift to 97 then we get lowercase
  (char (+ (rand 26) 97))

  ;; check str concat
  (str "bla" \c "blah")

  ;; add entropy
  (str-add "hello" 10)

  (generate-asym-true-result 10 10)
  (generate-false-result 10 10)) ;; End of rich comment block
