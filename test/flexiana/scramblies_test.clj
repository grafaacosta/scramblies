(ns flexiana.scramblies-test
  (:require [clojure.test :refer [deftest testing is]]
            [flexiana.test_utils :refer [generate-true-result generate-asym-true-result generate-false-result]]
            [flexiana.scramblies :refer [scramble?]]))

(deftest scramble-test
  (testing "First case"
    (is (=
         (scramble? "rekqodlw" "world")
         true)))
  (testing "Second case"
    (is (=
         (scramble? "cedewaraaossoqqyt" "codewars")
         true)))
  (testing "Third case"
    (is (=
         (scramble? "katas" "steak")
         false)))

  (testing "Edge case"
    (is (=
         (scramble? "cedewaraaossoqqyt" "")
         false)))

  (testing "Edge case 2"
    (is (=
         (scramble? "" "")
         true))))

(def runs 1000)
(def entropy 10)

(deftest generate-results
  (testing "True Base case"
    (is (=
         (apply scramble? (generate-true-result 10))
         true)))

  (testing "True Bulk case"
    (is (every?
         #(apply scramble? (generate-true-result %1))
         (range 1 runs))))

  (testing "True Asym Base case"
    (is (=
         (apply scramble? (generate-asym-true-result 10 3))
         true)))

  (testing "True Bulk case"
    (is (every?
         #(apply scramble? (generate-asym-true-result %1 entropy))
         (range 1 runs))))

  (testing "False Base case"
    (is (=
         (apply scramble? (generate-false-result 10 3))
         false)))

  (testing "False Bulk case"
    (is (every?
         (comp
          not
          #(apply scramble? (generate-false-result %1 entropy)))
         (range 1 runs)))))
